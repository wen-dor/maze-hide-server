import { WebSocketServer } from "ws";

const port = 8080;

const timelimit_sec = 300;

let lobbies = {};

let wss = new WebSocketServer({ port: port });

let player_count = 0;
let player_sockets = [];

// Delete empty lobbies with their last activity older than 10 minutes
setInterval(() => {
    Object.keys(lobbies).forEach((key) => {
        let lobby = lobbies[key];

        let last_interaction_with_lobby = lobby.last_interaction_with_lobby.getTime();
        let now = new Date().getTime();

        // console.log("Deleting empty lobbies", lobby, last_interaction_with_lobby, now);

        if (now > last_interaction_with_lobby + 10 * 60 * 1000 && lobby.players.length == 0) {
            delete lobbies[key];

            console.log("Yeet Lobby", key);
        }
    });
}, 60 * 1000);

function send_event_to_client(target_player_id, type, data) {
    let msg = JSON.stringify({
        type: type,
        data: data,
    });

    if (type != "position") console.log(`Sending data to client ${target_player_id}: ${msg}`);

    let target_player = player_sockets.find((ws) => ws.id == target_player_id);

    target_player.send(msg);
}

function generate_random_id(length) {
    let digits = "123456789";

    let id = "";
    var n_char = digits.length;

    for (let i = 0; i < length; i++) {
        id += digits.charAt(Math.floor(Math.random() * n_char));
    }

    return id;
}

function find_player_lobby(player_id) {
    let res = Object.entries(lobbies).find(([key, lobby]) => lobby.players.map((p) => p.id).includes(player_id));

    if (res == undefined) return null;

    return res[1];
}

wss.on("listening", () => {
    let address = wss.address();

    console.log(`Server started. Listening at ${address.address} on port ${address.port} using ${address.family}`);
});

wss.on("close", () => {
    console.log("Server stopped.");
});

// New connection with client established.
wss.on("connection", (player) => {
    player.id = `${new Date().getTime()}${generate_random_id(3)}`; // "Random" Player ID
    console.log(`New player connected: PlayerID: ${player.id}`);

    player_sockets.push(player); // Add new socket to list of player sockets.
    player_count++;

    send_event_to_client(player.id, "welcome_to_the_internet", {
        your_id: player.id,
    });

    // Handle incoming messages.
    player.on("message", (data) => {
        let packet_data = JSON.parse(data);

        let type = packet_data.type;

        if (type != "position") console.log(`Got data from client ${player.id}: ${data}`);

        // Handle events
        if (type == "create_lobby") create_lobby(player.id, packet_data.data);
        if (type == "join_lobby") join_lobby(player.id, packet_data.data);

        if (type == "request_game_start") request_game_start(player.id);

        if (type == "position") position_received(player.id, packet_data.data);
        if (type == "update_morphen") morphen_received(player.id, packet_data.data);
        if (type == "player_invisible") invisible_received(player.id);
        if (type == "caught_player") caught_player_received(player.id, packet_data.data);
        if (type == "stunned_player") stunned_player_received(player.id, packet_data.data);
        if (type == "spawn_messer") spawn_messer_received(player.id, packet_data.data);
    });

    player.on("close", () => {
        console.log(`Connection with player ${player.id} closed.`);

        // Remove socket from player_socket list
        let socket_index = player_sockets.indexOf(player);
        player_sockets.splice(socket_index, 1);

        player_count--;

        // Remove player from lobby
        let their_lobby_id = find_player_lobby(player.id)?.id;

        // No lobby id found (probably because player has not yet joined a lobby)
        if (their_lobby_id == undefined) return;
        let lobby = lobbies[their_lobby_id];

        let disconnected_player_meta = lobby.players.find((x) => x.id == player.id);
        let disconnected_player_index = lobby.players.indexOf(disconnected_player_meta);
        if (disconnected_player_index >= 0) lobby.players.splice(disconnected_player_index, 1);

        if (disconnected_player_meta.team == "sucher") lobby.sucher_count--;
        if (disconnected_player_meta.team == "verstecker") lobby.sucher_verstecker--;
        if (disconnected_player_meta.has_been_caught == true) lobby.caught_verstecker_count--;

        // Inform other players
        lobby.players.forEach((curr_player) => {
            if (curr_player.id == player.id) return;

            send_event_to_client(curr_player.id, "player_left", {
                id: player.id,
            });
        });
    });
});

function create_lobby(player_id, data) {
    let lobby_id = generate_random_id(3);

    lobbies[lobby_id] = {
        id: lobby_id,
        game_is_running: false,
        players: [],
        sucher_count: 0,
        verstecker_count: 0,
        caught_verstecker_count: 0,
        last_interaction_with_lobby: new Date(),
    };

    send_event_to_client(player_id, "lobby_created", {
        lobbyID: lobby_id,
    });
}

function join_lobby(player_id, data) {
    let lobby_id = data.lobbyID;
    let lobby = lobbies[lobby_id];

    // Abort if lobby does not exist
    if (lobby == undefined) return;

    // Block players from joining while game is running
    if (lobby.game_is_running == true) return;

    let team = "";
    let team_of_last_player_who_joined = lobby.players[lobby.players.length - 1]?.team ?? "verstecker";

    if (team_of_last_player_who_joined == "sucher") {
        team = "verstecker";
        lobby.verstecker_count += 1;
    } else {
        team = "sucher";
        lobby.sucher_count += 1;
    }

    // TODO: Abbrechen, wenn Spieler bereits in der Lobby ist

    lobby.players.push({
        id: player_id,
        team,
        has_been_caught: team == "verstecker" ? false : null,
    });

    send_event_to_client(player_id, "lobby_joined", {
        info: lobby,
        your_team: team,
    });

    // Spawn players
    let players = lobby.players;

    players.forEach((curr_player) => {
        if (curr_player.id == player_id) return;

        // Inform other players
        send_event_to_client(curr_player.id, "player_joined", {
            id: player_id,
            team: team,
        });

        // Tell new player about already present players
        send_event_to_client(player_id, "player_joined", {
            id: curr_player.id,
            team: curr_player.team,
        });
    });
}

function request_game_start(player_id) {
    let lobby = find_player_lobby(player_id);
    if (!lobby) return;

    // Require at least two players in lobby to start game
    // if (lobby.sucher_count <= 0 || lobby.verstecker_count <= 0) return;

    // Abort if game is already running
    if (lobby.game_is_running == true) return;

    lobby.game_is_running = true;

    lobby.players.forEach((curr_player) => {
        send_event_to_client(curr_player.id, "game_started", {
            verstecker_count: lobby.verstecker_count,
            sucher_count: lobby.sucher_count,
            caught_verstecker_count: lobby.caught_verstecker_count,
            rest_verstecker_count: lobby.verstecker_count - lobby.caught_verstecker_count,
            timelimit_sec,
        });
    });

    setTimeout(() => {
        end_game(lobby.id, "time_over");
    }, timelimit_sec * 1000);
}

function end_game(lobby_id, reason) {
    let lobby = lobbies[lobby_id];
    if (!lobby) return;

    let winning_team;
    let loosing_team;

    if (reason == "time_over") {
        winning_team = "verstecker";
        loosing_team = "sucher";
    } else if (reason == "all_verstecker_caught") {
        winning_team = "sucher";
        loosing_team = "verstecker";
    }

    lobby.players.forEach((curr_player) => {
        send_event_to_client(curr_player.id, "game_ended", {
            winning_team,
            loosing_team,
            reason,
        });
    });

    // Clear lobby
    lobby.players = [];
    lobby.sucher_count = 0;
    lobby.verstecker_count = 0;
    lobby.rest_verstecker_count = 0;
    lobby.caught_verstecker_count = 0;
    lobby.game_is_running = false;
    lobby.last_interaction_with_lobby = new Date();
}

function position_received(player_id, data) {
    let lobby = find_player_lobby(player_id);
    if (!lobby) return;

    let x = data.posx;
    let y = data.posy;
    let direc = data.direc;

    lobby.players.forEach((curr_player) => {
        if (curr_player.id == player_id) return;

        send_event_to_client(curr_player.id, "position", {
            id: player_id,
            posx: x,
            posy: y,
            direc,
        });
    });
}

function morphen_received(player_id, data) {
    let lobby = find_player_lobby(player_id);
    if (!lobby) return;

    let morph_object_path = data.path;
    let action = data.action;

    lobby.players.forEach((curr_player) => {
        if (curr_player.id == player_id) return;

        send_event_to_client(curr_player.id, "update_morphen", {
            id: player_id,
            path: morph_object_path,
            action,
        });
    });
}

function caught_player_received(player_id, data) {
    let lobby = find_player_lobby(player_id);
    if (!lobby) return;

    let caught_player_id = data.caught_player_id;
    let player = lobby.players.find((x) => x.id == caught_player_id);

    player.has_been_caught = true;
    lobby.caught_verstecker_count += 1;

    if (lobby.caught_verstecker_count >= lobby.verstecker_count) end_game(lobby.id, "all_verstecker_caught");

    lobby.players.forEach((curr_player) => {
        send_event_to_client(curr_player.id, "player_has_been_caught", {
            caught_player_id,
            rest_verstecker_count: lobby.verstecker_count - lobby.caught_verstecker_count,
        });
    });
}

function stunned_player_received(player_id, data) {
    let lobby = find_player_lobby(player_id);
    if (!lobby) return;

    let stunned_player_id = data.stunned_player_id;

    lobby.players.forEach((curr_player) => {
        send_event_to_client(curr_player.id, "player_has_been_stunned", {
            stunned_player_id,
        });
    });
}

function invisible_received(player_id) {
    let lobby = find_player_lobby(player_id);
    if (!lobby) return;

    lobby.players.forEach((curr_player) => {
        if (curr_player.id == player_id) return;

        send_event_to_client(curr_player.id, "player_invisible", {
            invisible_player_id: player_id,
        });
    });
}

function spawn_messer_received(player_id, data) {
    let lobby = find_player_lobby(player_id);
    if (!lobby) return;

    lobby.players.forEach((curr_player) => {
        send_event_to_client(curr_player.id, "messer_spawned", {
            owner_id: player_id,
            ...data,
        });
    });
}
